Coop-Control
############

Using micropython with ESP8285 board and ds18b20 temp probes

`MicroPyhton docs esp8266 <https://docs.micropython.org/en/latest/esp8266/index.html>`_

`Makerfocus 2pcs ESP8285 ESP-M2 CH340 Development Board WIFI Serial Port Module CH340 Compatible with ESP8266
<https://www.amazon.com/gp/product/B075K48R1Q/ref=oh_aui_detailpage_o00_s00?ie=UTF8&th=1>`_

Flashing
~~~~~~~~

This command worked to flash, needed to lower baud rate::

    esptool.py --port /dev/ttyUSB0 --baud 115200 write_flash --flash_size=detect --flash_mode=dout 0 bin/esp8266-20180511-v1.9.4.bin --verify

This command can be used to erase the flash (sometimes needed to fix file system issues)::

    esptool.py --port /dev/ttyUSB0 --baud 115200 erase_flash

To connect via serial use::

    picocom /dev/ttyUSB0 -b115200
    rshell --port /dev/ttyUSB0 --baud 115200

Pins
~~~~

- The pin for the base LED is pin 16.
- The pin for the LED on the ESP itself is pin 2.
- Both of these LEDs use active low, which means something like 
  ``digitalWrite(16, LOW)`` will turn the LED on, and vise versa.

ESP module
~~~~~~~~~~

`FCC page for chip <https://fccid.io/2AL3B-ESP-M>`_

`User Manual for chip <https://fccid.io/2AL3B-ESP-M/Users-Manual/User-Manual-3413969>`_

This is a development board for ESP-Mx (has the usb to power and flash the ESP8285.)
This means you can use ESP-M2 which has more io, or a ESP-M3 on the back (not included).
Things this is:

- ESP-M{1,2}
- ESP8285
- ESP8266 (just has onboard flash, same code)

Probes
~~~~~~

Using Diymore DS18B20 from `amazon <https://www.amazon.com/gp/product/B01JKVRVNI/ref=oh_aui_detailpage_o01_s00?ie=UTF8&psc=1>`_

Prob IDs::

    bytearray(b'(\xffK\r`\x17\x03\x89')
    bytearray(b'(\xff\x8d\x86$\x17\x03\xd5')
    bytearray(b'(\xff\x14u`\x17\x03\xc7')
    bytearray(b'(\xffP\r`\x17\x03\x85')
    bytearray(b'(\xff@d\x81\x14\x02\x9d')

Enviroment
~~~~~~~~~~

To test local download the micropyhon source and build it::

    cd ports/unix
    make axtls
    make

And to install and sym link it (didn't work for me)::

    sudo cp micropython /usr/local/bin/micropython
    sudo ln -s micropython /usr/local/bin/micropython


To update files I use either the webREPL_ or uPyLoader_


To make the icon::

    convert -background none -density 384 coop-control-logo.svg -define icon:auto-resize coop-control.ico

File Structure
~~~~~~~~~~~~~~

::

    .
    ├── bin (firmware)
    ├── src
    │   ├── full  (WIP that will be more structured)
    │   ├── simple  (current coop-control)
    │   ├── server_old.py  (old version of server that works)
    │   └── temp.py  (prints out temps)
    └── yaota8266  (https://github.com/pfalcon/yaota8266.git)


TODO
~~~~
See about having a `simple request class <https://gist.github.com/chrisguitarguy/1308286>`

Have a ``TempProbe`` class to handle offsets.

.. _uPyLoader: https://github.com/BetaRavener/uPyLoader/
.. _webREPL: http://micropython.org/webrepl/
