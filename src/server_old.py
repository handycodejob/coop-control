import onewire, ds18x20
import machine

# all pins
pins = [machine.Pin(i, machine.Pin.IN) for i in (0, 2, 4, 5, 12, 13, 14, 15)]
# the device is on GPIO2
dat = machine.Pin(2)
# create the onewire object
ds = ds18x20.DS18X20(onewire.OneWire(dat))

html = """<!DOCTYPE html>
<html>
    <head>
        <title>Coop Control!</title>
        <meta http-equiv="refresh" content="5" />
    </head>
    <body>
        <h1>Kelly's Chicken Coop</h1>
        <h2>Coop Temps</h2>
        <table border="1"> <tr><th>Pin</th><th>Value</th></tr> %s </table>
    </body>
</html>
"""

a = """
        <h2>ESP8266 Pins</h2>
        <table border="1"> <tr><th>Location</th><th>Temp</th></tr> %s </table>
"""

mapping = {
    b'\xd5': "Nest box",
    b'\xc7': "Run",
    b'\x9d': "Perch",
    b'\x89': "Not Used",
    b'\x85': "Not Used",
}

def clean_roms(roms):
    return [(mapping.get(bytes(rom)[-1:], rom), rom) for rom in roms]

def get_f(ds, rom):
    return ds.read_temp(rom[1]) * 1.8 + 32


import socket
addr = socket.getaddrinfo('0.0.0.0', 80)[0][-1]

s = socket.socket()
s.bind(addr)
s.listen(1)

print('listening on', addr)

def main():
    try:
        while True:
            cl, addr = s.accept()
            print('client connected from', addr)
            cl_file = cl.makefile('rwb', 0)
            while True:
                line = cl_file.readline()
                if not line or line == b'\r\n':
                    break

            # scan for devices on the bus
            roms = ds.scan()
            print('found devices:', roms)
            roms = clean_roms(roms)
            ds.convert_temp()
            temp_vals = ['<tr><td>%s</td><td>%.2f&deg;F</td></tr>' % (rom[0], get_f(ds, rom)) for rom in roms]
            pin_vals = ['<tr><td>%s</td><td>%d</td></tr>' % (str(pin), pin.value()) for pin in pins]
            response = html % ('\n'.join(temp_vals),)
            # response = html % ('\n'.join(temp_vals), '\n'.join(pin_vals))
            cl.send(response)
            cl.close()
    except KeyboardInterrupt:
        print("closing connection")
    except Exception as e:
        print(e)
    finally:
        s.listen(0)

main()
