from ds18x20 import DS18X20
from onewire import OneWire
from machine import Pin
from gc import collect as gc_collect
import socket

# the device is on GPIO2
device = Pin(2)
# create the onewire object
probes = DS18X20(OneWire(device))

connections = 0  # number of connections this boot
RESTART_AFTER = 120  # restart station after X connections

# Note line endings and trailing `\`
RESP_HEADER = """HTTP/1.1 200 OK\r\n\
Server: CoopControl-mircopython-HTTP-Server\r\n\
Content-Type: text/html\r\n\
\r\n\
"""

HTML_MAIN = """<!DOCTYPE html>
<html>
    <head>
        <title>Coop Control!</title>
    </head>
    <body>
        <h1>Kelly's Chicken Coop</h1>
        <h2>Coop Temps</h2>
        <table border="1">
            <tr>
                <th>Location</th>
                <th>Temperature</th>
            </tr>
            {}
        </table>
    </body>
</html>
"""

#
#         <div id="countdown"></div>
#         <div>
#             <input type="checkbox" id="refresh">
#             <label for="refresh">Don't refresh</label>
#         </div>
# <script src="https://cdn.rawgit.com/sydhenry/5adea07a512a311799b15c8b0e71e3fa/raw/0651ee398716348dec8fb17c56f8fc3aa8eb17c2/coop.js"></script>

HTML_ROW = """<tr><td>{}</td><td>{:.2f}&deg;F</td></tr>"""

mapping = {
    b'\xd5': "Nest box",
    b'\xc7': "Run",
    b'\x9d': "Coop",
    b'\x89': "Not Used",
    b'\x85': "Not Used",
}


def clean_id(ittr):
    """
    Itterate through each byte in array or byte in bytes and converts them to
    hex and joins them together
    """
    return "".join([hex(i) for i in ittr])

def clean_roms(roms):
    return [(mapping.get(bytes(rom)[-1:], rom), rom) for rom in roms]

def get_f(probes, rom):
    return probes.read_temp(rom[1]) * 1.8 + 32

def get_temps(probes):
    # scan for devices on the bus
    roms = probes.scan()
    if len(roms) == 0:
        # if we don't have any probes, we get an error
        print("ERROR: Could not find any devices")
        return [('NA', 0.0)]
    print("Found devices: {}".format(str(roms)))
    roms = clean_roms(roms)
    probes.convert_temp()
    # roms are (name, rom)
    # temps are (name, temp) tuple
    return [(rom[0], get_f(probes, rom),) for rom in roms]

def accept_handler(sock):
    global connections, probes
    # accept connection
    conn, addr = sock.accept()
    print("Client #{} connected from {}:{}".format(connections, *addr))
    conn_file = conn.makefile('rwb', 0)
    # read request headers
    while True:
        line = conn_file.readline()
        if not line or line == b'\r\n':
            break
    # make html
    response = HTML_MAIN.format('\n'.join([
        HTML_ROW.format(*temp)
        for temp
        in get_temps(probes)
    ]))
    conn.send(RESP_HEADER)
    conn.send(response)
    conn.close()
    if connections >= RESTART_AFTER:
        import machine
        machine.reset()
    else:
        connections += 1
        gc_collect()

# setup socket
sock = socket.socket()
def setup_sock(sock):
    PORT = 80
    server_addr = socket.getaddrinfo('0.0.0.0', PORT)[0][-1]
    sock.bind(server_addr)
    sock.listen(3)  # allow 3 connections
    # sock.settimeout(5)  # in seconds
    # sock.setblocking(False)
    # sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, accept_handler)
    print("Socket bound at {}:{}".format(*server_addr))
    return sock

def main():
    # we want to use the golabl sock so we can close it if needed.
    global sock, probes
    # get the temps once on load to prime them
    sock = setup_sock(sock)
    get_temps(probes)
    try:
        while True:
            accept_handler(sock)
    except KeyboardInterrupt:
        print("Stoped via KeyboardIntterrupt")
    finally:
        sock.close()

if __name__ == "__main__":
    main()
