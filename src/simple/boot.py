# This file is executed on every boot (including wake-boot from deepsleep)
#import esp
#esp.osdebug(None)
import gc
from micropython import mem_info
import webrepl

def server_restart():
    import sys
    try:
        del sys.modules['server']
    except KeyError:
        pass
    import server
    try:
        server.sock.close()
    except:
        print("Failed to close socket, maybe it isn't open?")
    server.main()

print("Starting WebRPEL")
webrepl.start()
print("Init garbage collection")
gc.collect()
mem_info()
print('Initial free: {} allocated: {}'.format(gc.mem_free(), gc.mem_alloc()))
