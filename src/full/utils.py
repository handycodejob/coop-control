import logging
from time import sleep
try:
    import umachine.PinBase as Pin
except ImportError:
    import machine.Pin as Pin
try:
    from ucollections import OrderedDict, deque
except ImportError:
    from collections import OrderedDict, deque

DEBUG = True
TIMEIT = DEBUG
FAIL_SAFE = DEBUG
LED_PIN = 16
JSON_CONF = {
    'indent': 2,
    'separators': (',', ': ',),
    'sort_keys': True,
    'cache_size': 10,
}
DEFAULT_CONF = {
    'led_pin': LED_PIN,
    'data_pin': 16,
    'devices': OrderedDict(),
    # 'devices': OrderedDict({
    #     b'\xffK\r`\x17\x03\x89': {
    #        'name': 'Pearch',
    #        'address': b'\xffK\r`\x17\x03\x89',
    #        'offset': 0,
    #     }
    # }),
}

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)


class LED(Pin):
    def __init__(self, no, dir=Pin.IN):
        working = False
        super().__init__(no, dir)

    def working(self, func):
        """
        Decorator that turns the LED on while the function is executing
        """
        def wrapper(*args, **kwargs):
            self.on()
            func(*args, **kwargs)
            self.off()
        return wrapper

    def using(self, func):
        def wrapper(*args, **kwargs):
            if self.working:
                log.warn("Led is currently working")
            self.working = True
            func(*args, **kwargs)
            self.working = False
        return wrapper

    @self.using
    def flash(self, t_on=10, t_off=10, number=100):
        """
        Not using pwm as pin 16 doesn't have that
        t_on - how long the led should be on
        t_off - how long the led should be off
        number - how many times to flash

        Always start with the LED off and end with it off to make sure
        that it will always default into that state
        """
        raise NotImplemented
        self.off()
        for _ in range(number):
            self.on()
            sleep(t_on)
            self.off()
            sleep(t_off)
        self.off()

led = LED(LED_PIN)


class Cache(deque):
    def clean(self):
        deque = self.q
        remove = len(deque) - config.get('cache_size')
        for _ in range(remove if remove > 0 else 0):
            deque.popright()
        self.q = deque

    def add(self, value):
        self.appendleft(value)
        self.clean()

    def get(self):
        return self.q[0]


def timed_function(f, active=TIMEIT, *args, **kwargs):
    func_name = str(f).split(' ')[1]
    def new_func(*args, **kwargs):
        t = utime.ticks_us()
        result = f(*args, **kwargs)
        delta = utime.ticks_diff(utime.ticks_us(), t)
        log.debug('Function {} Time = {:6.3f}ms'.format(myname, delta/1000))
        return result
    return new_func if active else f(*args, **kwargs)


def toggle(pin):
    """
    Set the pin to the value that it is not.
    """
    return pin.value(not pin.value())


@timed_function
def load_config(file_name, graceful=GRACEFUL):
    with open(file_name, 'r') as file:
        try:
            config = json.load(file)
        except OSError as err:
            config = DEFAULT_CONF
            if err.args[0] == 9:
                # [Errno 9] EBADF
                log.warning(f"'{file_name}' file not found, using default config")
            else:
                if graceful:
                    log.warning(f"graceful set, ignoring error `{err.args}` "
                                f"and using default config")
                else:
                    raise err
    return config


@timed_function
def save_config(file_name, config, graceful=GRACEFUL):
    old_conf = load_config(file_name)
    # get diff between the configs
    # https://stackoverflow.com/a/32815681
    conf_diff = json.dump({ k : config.get(k) for k in set(config) - set(old_conf) },
                          **JSON_CONF)
    file = open(file_name, 'w')
    try:
        config = json.dump(config, file, **JSON_CONF)
    except Exception as err:
        if graceful:
            log.warning(f"graceful set, ignoring error `{err.args}` "
                        f"and not saving the config")
    else:
        log.info(f"Config has been updated and changed "
                 f"the following:\n{conf_diff}")
    finally:
        file.close()
