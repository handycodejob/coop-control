import onewire
from ds18x20 import DS18X20
import machine
from __init__ import config
from utils import log

data = machine.Pin(TEMP_PEN)
# create the onewire object
OneWireInterface = ds18x20.DS18X20(onewire.OneWire(dat))

mapping = {
    b'\x89': "Nest box",
    b'\xd5': "Outside",
    b'\xc7': "Water",
    b'\x85': "Perch",
    b'\x9d': "Shade",
}

def TempratureProbe(object):
    """
    I want to have a list of probes. each probe needs a name, last temp,
        and the ablity to read the temp/ convert/write it as needed

    """
    PROBE_DEFAULTS = {
        'title': '',
        'description': '',
        'address': '',
        'offset': 0,
        'temps': [],
    }

    def __init__(self, interface, address, **kwargs):
        """
        kwargs are all passed to instance vars
        defaults -> config -> kwargs
        """
        self.interface = interface
        try:
            conf_data = config['devices'][address]
        except AttributeError:
            log.warn("Could not find device `{}`, "
                     "using defaults".format(address))
            conf_data = PROBE_DEFAULTS.copy()
        conf_data.update(kwargs)
        for key, val in conf_data.items():
            setattr(self, key, value)
        self.temps = Cache(self.temps)

    @property
    def temp(self):
        return self.temps.get()
    celsius = temp

    @property
    def fahrenheit(self):
        return self.temp * 1.8 + 32

    @property
    def name(self):
        return self.title if self.title else self.address

    def cache_temp(self, temp):
        self.temps.add(temp)

    def __dict__(self):
        return { key: getattr(self, key) in PROBE_DEFAULTS.keys() }

    def __str__(self):
        return "{}: {}°C".format(self.name, self.celsius)

def clean_roms(roms):
    return [(mapping.get(bytes(rom)[-1:], rom), rom) for rom in roms]

def get_f(ds, rom):
    return ds.read_temp(rom[1]) * 1.8 + 32


import socket
addr = socket.getaddrinfo('0.0.0.0', 80)[0][-1]

s = socket.socket()
s.bind(addr)
s.listen(1)

print('listening on', addr)

def main():
    try:
        while True:
            cl, addr = s.accept()
            print('client connected from', addr)
            cl_file = cl.makefile('rwb', 0)
            while True:
                line = cl_file.readline()
                if not line or line == b'\r\n':
                    break

            # scan for devices on the bus
            roms = ds.scan()
            print('found devices:', roms)
            roms = clean_roms(roms)
            ds.convert_temp()
            temp_vals = ['<tr><td>%s</td><td>%.2f&deg;F</td></tr>' % (rom[0], get_f(ds, rom)) for rom in roms]
            pin_vals = ['<tr><td>%s</td><td>%d</td></tr>' % (str(pin), pin.value()) for pin in pins]
            response = html % ('\n'.join(temp_vals),)
            # response = html % ('\n'.join(temp_vals), '\n'.join(pin_vals))
            cl.send(response)
            cl.close()
    except KeyboardInterrupt:
        print("closing connection")
    except Exception as e:
        print(e)
    finally:
        s.listen(0)

main()
