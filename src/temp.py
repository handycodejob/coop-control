import time
import machine
import onewire, ds18x20

# the device is on GPIO2
dat = machine.Pin(2)

# create the onewire object
ds = ds18x20.DS18X20(onewire.OneWire(dat))

# scan for devices on the bus
roms = ds.scan()
print('found devices:', roms)

# loop 10 times and print all temperatures
def main():
    for i in range(10):
        print('temperatures:')
        ds.convert_temp()
        time.sleep_ms(750)
        for rom in roms:
            print("{}: {}".format(rom, ds.read_temp(rom)))
        print()
